﻿using Microsoft.EntityFrameworkCore;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using todo_domain_entities.Contexts;
using todo_domain_entities.Models;
using todo_domain_entities.Repositories;
using Xunit;

namespace todo_app_tests
{
    public class TodoList_Tests
    {
        private readonly DbContextOptions<ToDoContext> options;

        public TodoList_Tests()
        {
            options = new DbContextOptionsBuilder<ToDoContext>()
                .UseInMemoryDatabase(databaseName: "ToDoAppDb")
                .Options;
        }

        [Fact]
        public void ToDoListRepositoryGetAll_ReturnsAllToDoLists_ReturnsAllInsertedData()
        {
            // Arrange
            List<ToDoList> toDoLists = new List<ToDoList>
            {
                new ToDoList { Id = 1, Name = "Homework" },
                new ToDoList { Id = 2, Name = "Shopping" },
                new ToDoList { Id = 3, Name = "Housework" }
            };

            using (var db = new ToDoContext(options))
            {
                db.ToDoLists.AddRange(toDoLists);
                db.SaveChanges();                
            }

            using (var db = new ToDoContext(options))
            {
                // Act
                IRepository<ToDoList> repository = new ToDoListRepository(db);
                List<ToDoList> allToDoLists = repository.GetAll().ToList();

                // Assert
                Assert.Equal(3, allToDoLists.Count);
                
                // Database cleanup
                db.Database.EnsureDeleted();
            }            
        }

        [Fact]
        public void ToDoListRepositoryGetById_ReturnsToDoList_ReturnsToDoListWithTheGivenId()
        {
            // Arrange
            ToDoList toDoList = new ToDoList { Id = 1, Name = "Homework" };

            using (var db = new ToDoContext(options))
            {
                db.Add(toDoList);
                db.SaveChanges();
            }

            using (var db = new ToDoContext(options))
            {
                // Act
                IRepository<ToDoList> repository = new ToDoListRepository(db);
                var result = repository.GetById(toDoList.Id);

                // Assert
                Assert.IsType<ToDoList>(result);
                Assert.Equal(toDoList.Id, result.Id);

                // Database cleanup
                db.Database.EnsureDeleted();
            }
        }

        [Fact]
        public void ToDoListRepositoryGetById_ReturnsNull_NonExistingToDoList()
        {
            // Arrange
            ToDoList toDoList = new ToDoList { Id = 1, Name = "ToDoNonExisting" };

            using (var db = new ToDoContext(options))
            {
                // Act
                IRepository<ToDoList> repository = new ToDoListRepository(db);
                var result = repository.GetById(toDoList.Id);

                // Assert
                Assert.Null(result);

                // Database cleanup
                db.Database.EnsureDeleted();
            }
        }

        [Fact]
        public void ToDoListRepositoryCreate_ReturnsCreatedObject_NormalToDoList()
        {
            // Arrange
            ToDoList toDoList = new ToDoList { Id = 1, Name = "Studying" };

            using (var db = new ToDoContext(options))
            {
                // Act
                IRepository<ToDoList> repository = new ToDoListRepository(db);
                var result = repository.Create(toDoList);

                // Assert
                Assert.NotNull(result);
                Assert.Equal(toDoList.Id, result.Id);


                // Database cleanup
                db.Database.EnsureDeleted();
            }
        }

        [Fact]
        public void ToDoListRepositoryDelete_DeletesToDoList_ReturnsVoid()
        {
            // Arrange
            ToDoList toDoList = new ToDoList { Id = 1, Name = "Homework" };

            using (var db = new ToDoContext(options))
            {
                db.Add(toDoList);
                db.SaveChanges();
            }

            using (var db = new ToDoContext(options))
            {
                // Act
                IRepository<ToDoList> repository = new ToDoListRepository(db);
                repository.Delete(toDoList.Id);

                // Assert
                Assert.Null(repository.GetById(toDoList.Id));
                Assert.Empty(repository.GetAll().ToList());

                // Database cleanup
                db.Database.EnsureDeleted();
            }
        }

        [Fact]
        public void ToDoListRepositoryDelete_ThrowsArgumentNullException_TriesToDeleteNonExistingElement()
        {
            using (var db = new ToDoContext(options))
            {
                // Arrange
                IRepository<ToDoList> repository = new ToDoListRepository(db);
                

                // Act & Assert
                Assert.Throws<ArgumentNullException>(() => repository.Delete(int.MaxValue));

                // Database cleanup
                db.Database.EnsureDeleted();
            }
        }

        [Fact]
        public void ToDoListRepositoryUpdate_UpdatesTheExistingItem_ReturnTrueIfSuccess()
        {
            // Arrange
            ToDoList toDoList = new ToDoList { Id = 1, Name = "Homework" };

            using (var db = new ToDoContext(options))
            {
                db.Add(toDoList);
                db.SaveChanges();
            }

            using (var db = new ToDoContext(options))
            {
                // Act
                IRepository<ToDoList> repository = new ToDoListRepository(db);
                toDoList.Name = "Housework";
                bool updated = repository.Update(toDoList);
                ToDoList toDoUpdated = repository.GetById(toDoList.Id);

                // Assert
                Assert.True(updated);
                Assert.NotEqual("Homework", toDoUpdated.Name);
                Assert.Equal("Housework", toDoUpdated.Name);

                // Database cleanup
                db.Database.EnsureDeleted();
            }
        }
    }
}
