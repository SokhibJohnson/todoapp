﻿using System;
using System.ComponentModel.DataAnnotations;

namespace todo_domain_entities.Models
{
    public class ToDoEntity
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }
        
        [MaxLength(200)]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        
        public DateTime DueDate { get; set; }
        
        public DateTime CreationDate { get; set; } = DateTime.Now;

        public int Status { get; set; } = 0;

        public int ToDoListId { get; set; }
        public ToDoList ToDoList { get; set; }
    }
}
