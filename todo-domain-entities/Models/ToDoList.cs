﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace todo_domain_entities.Models
{
    public class ToDoList
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        public List<ToDoEntity> Entities { get; set; } = new List<ToDoEntity>();
    }
}
