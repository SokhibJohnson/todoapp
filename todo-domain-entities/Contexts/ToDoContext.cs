﻿using Microsoft.EntityFrameworkCore;
using todo_domain_entities.Models;

namespace todo_domain_entities.Contexts
{
    public class ToDoContext : DbContext
    {
        public ToDoContext(DbContextOptions<ToDoContext> options) : 
            base(options)
        {

        }

        public DbSet<ToDoList> ToDoLists { get; set; }
        public DbSet<ToDoEntity> ToDoEntities { get; set; }
    }
}
