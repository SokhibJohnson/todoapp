﻿using System.Collections.Generic;
using System.Linq;
using todo_domain_entities.Contexts;
using todo_domain_entities.Models;

namespace todo_domain_entities.Repositories
{
    public class ToDoListRepository : IRepository<ToDoList>
    {
        private readonly ToDoContext db;

        public ToDoListRepository(ToDoContext db)
        {
            this.db = db;
        }

        public ToDoList Create(ToDoList newEntity)
        {
            db.ToDoLists.Add(newEntity);
            db.SaveChanges();

            return newEntity;
        }

        public void Delete(int id)
        {
            ToDoList deletingEntity = db.ToDoLists.Find(id);
            db.ToDoLists.Remove(deletingEntity);
            db.SaveChanges();
        }

        public IEnumerable<ToDoList> GetAll()
        {
            return db.ToDoLists.ToList();
        }

        public ToDoList GetById(int id)
        {
            return db.ToDoLists.Find(id);
        }

        public bool Update(ToDoList updatingEntity)
        {
            if (updatingEntity is null)
            {
                return false;
            }

            db.Entry(db.ToDoLists.FirstOrDefault(s => s.Id == updatingEntity.Id)).CurrentValues.SetValues(updatingEntity);
            db.SaveChanges();

            return true;
        }
    }
}
