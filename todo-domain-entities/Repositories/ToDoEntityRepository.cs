﻿using System.Collections.Generic;
using System.Linq;
using todo_domain_entities.Contexts;
using todo_domain_entities.Models;

namespace todo_domain_entities.Repositories
{
    public class ToDoEntityRepository : IRepository<ToDoEntity>
    {
        private readonly ToDoContext db;

        public ToDoEntityRepository(ToDoContext db)
        {
            this.db = db;
        }

        public ToDoEntity Create(ToDoEntity newEntity)
        {
            db.ToDoEntities.Add(newEntity);
            db.SaveChanges();

            return newEntity;
        }

        public void Delete(int id)
        {
            ToDoEntity deletingEntity = db.ToDoEntities.Find(id);
            db.ToDoEntities.Remove(deletingEntity);
            db.SaveChanges();
        }

        public IEnumerable<ToDoEntity> GetAll()
        {
            return db.ToDoEntities.ToList();
        }

        public ToDoEntity GetById(int id)
        {
            return db.ToDoEntities.Find(id);
        }

        public bool Update(ToDoEntity updatingEntity)
        {
            if (updatingEntity is null)
            {
                return false;
            }

            db.Entry(db.ToDoEntities.FirstOrDefault(s => s.Id == updatingEntity.Id)).CurrentValues.SetValues(updatingEntity);
            db.SaveChanges();

            return true;
        }
    }
}
