﻿using System.Collections.Generic;

namespace todo_domain_entities.Repositories
{
    public interface IRepository<TEntity>
    {
        TEntity GetById(int id);
        IEnumerable<TEntity> GetAll();
        TEntity Create(TEntity newEntity);
        void Delete(int id);
        bool Update(TEntity updatingEntity);
    }
}
