﻿using System.Collections.Generic;
using todo_domain_entities.Models;

namespace todo_aspnetmvc_ui.Models
{
    public class ToDoViewModel
    {
        public ToDoList ToDoList { get; set; }
        public List<ToDoEntity> ToDoEntities { get; set; }
    }
}
