﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_aspnetmvc_ui.Models;
using todo_domain_entities.Models;
using todo_domain_entities.Repositories;

namespace todo_aspnetmvc_ui.Controllers
{
    public class ToDoEntityController : Controller
    {
        private readonly IRepository<ToDoEntity> todoentityRepo;
        private readonly IRepository<ToDoList> todolistrepo;

        public ToDoEntityController(IRepository<ToDoEntity> repository, IRepository<ToDoList> todolistrepo)
        {
            this.todoentityRepo = repository;
            this.todolistrepo = todolistrepo;
        }
        
        [HttpGet]
        public IActionResult Create(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        [HttpPost]
        public IActionResult Create(IFormCollection collection)
        {
            int todoListId = Convert.ToInt32(collection["ToDoListId"]);
            ToDoEntity toDoEntity = new ToDoEntity
            {
                Title = collection["Title"],
                Description = collection["Description"],
                DueDate = Convert.ToDateTime(collection["DueDate"]),
                CreationDate = DateTime.Now,
                Status = Convert.ToInt32(collection["Status"]),
                ToDoListId = todoListId
            };

            todoentityRepo.Create(toDoEntity);
            return RedirectToAction(actionName: "Details", controllerName: "ToDoList", new { id = todoListId });
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            return View(todoentityRepo.GetById(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                int todoListId = todoentityRepo.GetById(id).ToDoListId;
                todoentityRepo.Delete(id);
                
                return RedirectToAction(actionName: "Details", controllerName: "ToDoList", new { id = todoListId });
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            return View(todoentityRepo.GetById(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            ToDoEntity toDoEntity = todoentityRepo.GetById(id);

            try
            {
                toDoEntity.Title = collection["Title"];
                toDoEntity.Description = collection["Description"];
                toDoEntity.DueDate = Convert.ToDateTime(collection["DueDate"]);
                toDoEntity.Status = Convert.ToInt32(collection["Status"]);

                todoentityRepo.Update(toDoEntity);

                return RedirectToAction(actionName: "Details", controllerName: "ToDoList", new { id = toDoEntity.ToDoListId});
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            return View(todoentityRepo.GetById(id));
        }

    }
}
