﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using todo_aspnetmvc_ui.Models;
using todo_domain_entities.Contexts;
using todo_domain_entities.Models;
using todo_domain_entities.Repositories;

namespace todo_aspnetmvc_ui.Controllers
{
    public class ToDoListController : Controller
    {
        private readonly IRepository<ToDoList> todolistRepo;
        private readonly IRepository<ToDoEntity> todoentityRepo;

        public ToDoListController(IRepository<ToDoList> repository, IRepository<ToDoEntity> todoentityRepo)
        {
            this.todolistRepo = repository;
            this.todoentityRepo = todoentityRepo;
        }

        [HttpGet]
        public IActionResult Index()
        {
            List<ToDoViewModel> toDoViews = new List<ToDoViewModel>();
            foreach (ToDoList toDoList in todolistRepo.GetAll())
            {
                toDoViews.Add(
                    new ToDoViewModel
                    {
                        ToDoList = toDoList,
                        ToDoEntities = todoentityRepo.GetAll().Where(e => e.ToDoListId == toDoList.Id).ToList()
                    });
            }

            return View(toDoViews);
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                ToDoList toDoList = new ToDoList {
                    Name = collection["Name"],
                    Entities = new List<ToDoEntity>()
                };
                todolistRepo.Create(toDoList);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            ToDoList toDoList = todolistRepo.GetById(id);
            return View(toDoList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            ToDoList toDoList = todolistRepo.GetById(id);
            
            try
            {
                toDoList.Name = collection["Name"];
                todolistRepo.Update(toDoList);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            return View(todolistRepo.GetById(id));
        }

        [HttpPost]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                todolistRepo.Delete(id);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            ToDoList toDoList = todolistRepo.GetById(id);

            var entities = todoentityRepo.GetAll().Where(e => e.ToDoListId == id).ToList();

            return View(new ToDoViewModel { ToDoList = toDoList, ToDoEntities = entities});
        }
        
    }
}
